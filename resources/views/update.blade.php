@extends('layouts.app');
@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="row">
            <form action="{{url("", $id)}}" method="POST">
                    @csrf
                    @method('PUT')

                    @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            Please fix the following errors
                        </div>
                    @endif
                        <div class="form-group">
                            <label for="">SAMPLE</label>
                            <input type="text" name="" class="form-control @error('') is-invalid @enderror" value="{{ old('')}}">
                            @error('title')
                                <div class="invalid-feedback"> {{ $message}} </div>
                            @enderror
                        </div>
                    <button class="btn btn-primary" type="submit">submit</button>

                </form>
            </div>
        </div>
    </div>
@endsection