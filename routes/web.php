<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
 
    return view('welcome');
});

Route::get('/submit', function () {
    return view('submit');
});

Route::post('/submit', function (Request $request) {


    return redirect('/');
});

Route::get('/update/{id}', function ($id) {
    return view('update', ['id' => $id]);
});

Route::put('update/{id}', function (Request $request, $id) {

    return redirect('/');

});

Route::delete('/{id}', function ($id) {
 
    return redirect('/');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
